#!/usr/bin/env bash

if ! [ -z "$GITLAB_SSH_KEY" ]; then
    eval $(ssh-agent)
    echo -e "$GITLAB_SSH_KEY" > /root/.ssh/gitlab_id_rsa
    chown -R root:root /root/.ssh
    chmod 600 /root/.ssh/gitlab_id_rsa
    ssh-add /root/.ssh/gitlab_id_rsa
fi

if ! [ -z "$GITHUB_API_TOKEN" ]; then
    composer config --global github-oauth.github.com $GITHUB_API_TOKEN
fi

composer install

./phing build-ci