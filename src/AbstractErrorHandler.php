<?php

namespace Multoo\ErrorHandler;

abstract class AbstractErrorHandler extends AbstractHandler
{

    public function toMsg($errno, $errstr, $file, $line)
    {
        $errors = array(
            1 => 'E_ERROR',
            2 => 'E_WARNING',
            4 => 'E_PARSE',
            8 => 'E_NOTICE',
            16 => 'E_CORE_ERROR',
            32 => 'E_CORE_WARNING',
            64 => 'E_COMPILE_ERROR',
            128 => 'E_COMPILE_WARNING',
            256 => 'E_USER_ERROR',
            512 => 'E_USER_WARNING',
            1024 => 'E_USER_NOTICE',
            6143 => 'E_ALL',
            2048 => 'E_STRICT',
            4096 => 'E_RECOVERABLE_ERROR',
            8192 => 'E_DEPRECATED',
            16384 => 'E_USER_DEPRECATED');

        $msg = "<b>" . $errors[$errno] . " [" . $errno . "]:</b> " . $errstr . "<br />" . PHP_EOL;
        $msg.= "\t <em>" . str_replace((defined('ROOT_DIR') ? ROOT_DIR : ""), "", $file) . ", line: " . $line . "</em>";

        $msg = $this->appendServerAndClientInfo($msg);

        return $msg;
    }
}
