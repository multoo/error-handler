<?php

namespace Multoo\ErrorHandler;

class Generic
{

    /**
     *
     * @var ErrorHandlerInterface
     */
    protected static $errorHandler;

    /**
     *
     * @var FatalErrorHandlerInterface
     */
    protected static $fatalErrorHandler;

    /**
     *
     * @var ThrowableHandlerInterface
     */
    protected static $throwableHandler;

    /**
     *
     * @var boolean
     */
    protected static $initialized = false;

    public function __construct($init = true, callable $logger = null, callable $killer = null)
    {
        self::init($init, $logger, $killer);
    }

    /**
     *
     * @param boolean $init
     * @param callable $logger
     * @param callable $killer
     */
    public static function init($init = true, callable $logger = null, callable $killer = null)
    {
        if (!self::$initialized) {
            self::$errorHandler = new \Multoo\ErrorHandler\ErrorHandler($init, $logger, $killer);
            self::$fatalErrorHandler = new \Multoo\ErrorHandler\FatalErrorHandler($init, $logger, $killer);
            self::$throwableHandler = new \Multoo\ErrorHandler\ThrowableHandler($init, $logger, $killer);
            self::$initialized = true;
        }
    }

    /**
     *
     * @param callable $killer
     * @return \Multoo\ErrorHandler\AbstractGeneric
     */
    public static function setKiller(callable $killer = null)
    {
        self::init();

        self::$errorHandler->setKiller($killer);
        self::$fatalErrorHandler->setKiller($killer);
        self::$throwableHandler->setKiller($killer);

        return $this;
    }

    /**
     *
     * @param ErrorHandlerInterface $errorHandler
     * @return Generic
     */
    public static function setErrorHandler(ErrorHandlerInterface $errorHandler)
    {
        self::$errorHandler = $errorHandler;
        return self;
    }

    /**
     *
     * @param FatalErrorHandlerInterface $fatalErrorHandler
     * @return Generic
     */
    public static function setFatalErrorHandler(FatalErrorHandlerInterface $fatalErrorHandler)
    {
        self::$fatalErrorHandler = $fatalErrorHandler;
        return self;
    }

    /**
     *
     * @param ThrowableHandlerInterface $throwableHandler
     * @return Generic
     */
    public static function setThrowableHandler(ThrowableHandlerInterface $throwableHandler)
    {
        self::$throwableHandler = $throwableHandler;
        return self;
    }

    /**
     *
     * @return ErrorHandlerInterface
     */
    public static function getErrorHandler()
    {
        return self::$errorHandler;
    }

    /**
     *
     * @return FatalErrorHandlerInterface
     */
    public static function getFatalErrorHandler()
    {
        return self::$fatalErrorHandler;
    }

    /**
     *
     * @return ThrowableHandlerInterface
     */
    public static function getThrowableHandler()
    {
        return self::$throwableHandler;
    }
}
