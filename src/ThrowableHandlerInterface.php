<?php

namespace Multoo\ErrorHandler;

use Throwable;

interface ThrowableHandlerInterface
{

    public function __construct($init = true, callable $logger = null, callable $killer = null);

    public function init();

    public function process(Throwable $throwable);

    public function toMsg(Throwable $throwable);

    public function log($throwable);

    public function setLogger(callable $logger = null);

    public function kill();

    public function setKiller(callable $killer = null);
}
