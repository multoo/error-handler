<?php

namespace Multoo\ErrorHandler;

use Throwable;

class ThrowableHandler extends \Multoo\ErrorHandler\AbstractHandler implements ThrowableHandlerInterface
{

    public function init()
    {
        set_exception_handler([$this, 'process']);
    }

    /**
     *
     * @param Throwable $throwable
     */
    public function process(Throwable $throwable)
    {
        $this->log($throwable);
        $this->kill();
    }

    public function toMsg(Throwable $throwable)
    {
        $trace = $throwable->getTrace();

        $msg = "<b>Uncaught throwable '" . get_class($throwable) . "':</b> " . $throwable->getMessage() . "<br />" . PHP_EOL;
        $msg.= "\t <em>" . str_replace((defined('ROOT_DIR') ? ROOT_DIR : ""), "", $throwable->getFile()) . ", line: " . $throwable->getLine() . "</em>";
        $msg.= "<br />" . PHP_EOL . "<em>TRACE:";

        foreach ($trace as $traceLine) {
            $msg.= "<br />" . PHP_EOL . "\t";
            if (isset($traceLine['class']) && $traceLine['class'] != '') {
                $msg.= $traceLine['class'];
                $msg.= "->";
            }
            $msg.= $traceLine['function'];
            $msg.= "();";
            if (isset($traceLine['file']) && isset($traceLine['line'])) {
                $msg.= "- " . str_replace((defined('ROOT_DIR') ? ROOT_DIR : ""), "", $traceLine['file']) . ", line: " . $traceLine['line'];
            }
        }
        $msg.="</em>";

        $msg = $this->appendServerAndClientInfo($msg);

        return $msg;
    }

    /**
     *
     * @param Throwable $throwable
     */
    public function log($throwable)
    {
        if (!$throwable instanceof Throwable) {
            throw new Throwable('Multoo\ErrorHandler\ThrowableHandler::log first param must be of type \'Throwable\'');
        }

        $msg = $this->toMsg($throwable);
        parent::log($msg);
    }
}
