<?php

namespace Multoo\ErrorHandler;

interface ErrorHandlerInterface
{

    public function __construct($init = true, callable $logger = null, callable $killer = null);

    public function init();

    public function toMsg($errno, $errstr, $file, $line);

    public function log($msg);

    public function setLogger(callable $logger = null);

    public function kill();

    public function setKiller(callable $killer = null);
}
