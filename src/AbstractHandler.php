<?php

namespace Multoo\ErrorHandler;

abstract class AbstractHandler
{

    /**
     *
     * @var callable
     */
    public static $logger;

    /**
     *
     * @var callable
     */
    public static $killer;

    /**
     *
     * @param boolean $init
     * @param callable $logger
     * @param callable $killer
     */
    public function __construct($init = true, callable $logger = null, callable $killer = null)
    {

        $this->setLogger($logger);
        $this->setKiller($killer);

        if ($init) {
            $this->init();
        }
    }

    abstract public function init();

    /**
     *
     * @param string $msg
     * @return string
     */
    public function appendServerAndClientInfo($msg)
    {
        if (isset($_SERVER['HTTP_HOST'])) {
            $msg .= "<br />" . PHP_EOL . "<em>HTTP_HOST: " . $_SERVER['HTTP_HOST'] . "</em>";
        }

        if (isset($_SERVER['HTTP_REFERER'])) {
            $msg .= "<br />" . PHP_EOL . "<em>HTTP_REFERER: " . htmlspecialchars($_SERVER['HTTP_REFERER']) . "</em>";
        }

        if (isset($_SERVER['REQUEST_URI'])) {
            $msg .= "<br />" . PHP_EOL . "<em>REQUEST_URI: " . (isset($_SERVER['HTTP_HOST']) ? "http" . (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 's' : null) . "://" . $_SERVER['HTTP_HOST'] : null) . htmlspecialchars($_SERVER['REQUEST_URI']) . "</em>";
        }

        if (isset($_SERVER['HTTP_USER_AGENT'])) {
            $msg .= "<br />" . PHP_EOL . "<em>HTTP_USER_AGENT: " . $_SERVER['HTTP_USER_AGENT'] . "</em>";
        }

        if (function_exists('getRealIpAddr') && ($ip = getRealIpAddr())) {
            $msg .= "<br />" . PHP_EOL . "<em>CLIENT_IP_ADDRESS: " . $ip . "</em>";
        }

        if (isset($_GET)) {
            $msg .= "<br />" . PHP_EOL . "<em>GET: " . print_r($_GET, true) . "</em>";
        }

        if (isset($_POST)) {
            $msg .= "<br />" . PHP_EOL . "<em>POST: " . print_r($_POST, true) . "</em>";
        }

        if (isset($_SESSION)) {
            $session = $_SESSION;
            unset($session['flashdata']);
            $msg .= "<br />" . PHP_EOL . "<em>SESSION: " . print_r($session, true) . "</em>";
        }

        return $msg;
    }

    public function log($msg)
    {
        error_log(strip_tags($msg), 0);

        if (is_callable(self::$logger)) {
            $callable = self::$logger;
            $callable($msg);
        }
    }

    /**
     *
     * @param callable $logger
     * @return \Multoo\ErrorHandler\AbstractHandler
     */
    public function setLogger(callable $logger = null)
    {
        self::$logger = $logger;
        return $this;
    }

    public function kill()
    {
        if (is_callable(self::$killer)) {
            $callable = self::$killer;
            $callable();
        }

        http_response_code(500);
        exit('KILLED BY ' . get_class($this) . '::kill()');
    }

    /**
     *
     * @param callable $killer
     * @return \Multoo\ErrorHandler\AbstractHandler
     */
    public function setKiller(callable $killer = null)
    {
        self::$killer = $killer;
        return $this;
    }
}
