<?php

namespace Multoo\ErrorHandler;

class ErrorHandler extends AbstractErrorHandler implements ErrorHandlerInterface
{

    public $killErrno = array(1, 16, 64, 128, 256); // 32 geeft SQL Lite error
    public $ignoreErrno = array(8, 2048);
    public $ignoreStrings = array();

    public function init()
    {
        set_error_handler([$this, 'process']);
    }

    /**
     *
     * @param int $errno
     * @param string $errstr
     * @param string $file
     * @param string $line
     * @return boolean
     */
    public function process($errno, $errstr, $file, $line)
    {

        if (error_reporting() != 0 // if error has been supressed with an @
                && !in_array($errno, $this->ignoreErrno) && !in_array($errstr, $this->ignoreStrings)
        ) {
            $msg = $this->toMsg($errno, $errstr, $file, $line);

            $this->log($msg);

            if (in_array($errno, $this->killErrno)) {
                $this->kill();
            }
        }

        return true;
    }
}
