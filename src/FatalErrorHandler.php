<?php

namespace Multoo\ErrorHandler;

class FatalErrorHandler extends AbstractErrorHandler implements FatalErrorHandlerInterface
{

    public function init()
    {
        register_shutdown_function([$this, 'process']);
    }

    public function process()
    {
        if (@is_array($e = @error_get_last())) {
            $errno = isset($e['type']) ? $e['type'] : 0;
            $errstr = isset($e['message']) ? $e['message'] : '';
            $file = isset($e['file']) ? $e['file'] : '';
            $line = isset($e['line']) ? $e['line'] : '';
            if ($errno > 0) {
                $msg = $this->toMsg($errno, $errstr, $file, $line);
                $this->log($msg);
                $this->kill();
            }
        }
    }
}
